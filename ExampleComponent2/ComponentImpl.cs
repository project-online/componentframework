﻿using ComponentFramework;
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleComponent2
{
    public class ComponentImpl : ComponentBaseImpl
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ComponentImpl() : base("ExampleComponent2", new string[] { "ExampleComponent", "ExampleComponent3" })
        { 
        }

        public override void OnLoad()
        {


        } 

        public override void OnStart()
        {
            NewMethod();
        }

        private static void NewMethod()
        {
            log.Debug("DynamicAPI Test");
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            ComponentManager.InvokeComponentAPI("ExampleComponent", "WowAPI");
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00} {4}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds, ts.Ticks);
            log.Debug(elapsedTime);
        }
    }
}
