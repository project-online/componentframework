﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentFramework.ScriptSchedulers.Example
{
    internal class RerunScript : ComponentScript
    {
        public RerunScript() : base("RerunScript", true)
        {
        }

        public override async Task OnExecute()
        {
            Console.WriteLine("RerunScript");

        }
    }
}
