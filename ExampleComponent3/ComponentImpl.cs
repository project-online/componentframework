﻿using ComponentFramework;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleComponent
{
    public class ComponentImpl : ComponentBaseImpl
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ComponentImpl() : base("ExampleComponent3")
        { 

        }

        public override void OnLoad()
        {
            Component.EventManager.RegisterEvent("EventHandler", new Action<string>((arg) =>
            {
                log.Debug(arg);
            }));

        } 

        public override void OnStart()
        {
            // Once all components are loaded, all components simulatiously start
        }
    }
}
