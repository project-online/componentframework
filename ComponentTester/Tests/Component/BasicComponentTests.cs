﻿using ComponentFramework;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentTester.Tests.Component
{
    [TestFixture]
    public class BasicComponentTests
    {
        [Test]
        public void BasicStartNewComponentSuccess()
        {
            var component = ComponentManager.StartComponent("ExampleComponent"); 

            Assert.NotNull(component); 
        }

        [Test]
        public void BasicStartNewComponentsSuccess()
        {
            var components = ComponentManager.StartComponents(new string[] { "ExampleComponent", "ExampleComponent2", "ExampleComponent3" });

            Assert.NotNull(components);
            Assert.AreEqual(components.Length, 3);
        }

        [Test]
        public void BasicStartNewComponentFailure()
        { 
            Assert.Throws<FileNotFoundException>(()=> { ComponentManager.StartComponent("ExampleComponent4"); });
        }
    }
}
