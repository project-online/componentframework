﻿using ComponentFramework.Managers;  
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ComponentFramework
{
    public enum ComponentStartProcess
    {
        INDIVIDUAL_START,
        SIMULTANEOUS_START,
    }

    #region Static
    public abstract class ComponentManager 
    {
        protected ComponentManager(IComponentAPIManager apiManager, IComponentEventManager eventManager, IComponentCommandManager commandManager)
        {
            _apiManager = apiManager;
            _eventManager = eventManager;
            _commandManager = commandManager;

            _components = new Dictionary<string, ComponentContainer>();

            _instance = this;
        }


        private bool HasComponents()
        {
            return _components == null || _components.Count == 0;
        }


        // we need somewhere to store components that are loaded in
        private Dictionary<string, ComponentContainer> _components;
        private IComponentAPIManager _apiManager;
        private IComponentEventManager _eventManager;
        private IComponentCommandManager _commandManager;

        public IComponentAPIManager ApiManager { get => _apiManager; }
        public IComponentEventManager EventManager { get => _eventManager; }
        public IComponentCommandManager CommandManager { get => _commandManager; }
        internal Dictionary<string, ComponentContainer> Components { get => _components; }
        public static bool IsSetup => _instance != null;

        private static ComponentManager _instance;

        internal static ComponentManager GetInstance()
        {
            return _instance;
        }


        public static void InvokeComponentEvent(string eventName, params object[] args)
        {
            var components = GetComponentNames();
            foreach (var item in components)
            {
                var component = GetComponent(item);
                InvokeComponentEvent(component, eventName, args);
            } 
        }

        public static void InvokeComponentCommand(string componentName, string commandName, params object[] args)
        {
            var component = GetComponent(componentName);
            component.CommandManager.InvokeCommand(commandName, args);
        }


        // This is just a basic method to ensure that people are forced to call the singular GetComponent to actually get the component object
        public static IEnumerable<string> GetComponentNames()
        {
            if (!GetInstance().HasComponents())
                return new string[0];

            return GetInstance().Components.Keys;
        }

        //public static void ExecuteScripts(string scriptName)
        //{
        //    foreach (var item in GetComponentNames())
        //    {
        //        var component = GetComponent(item);
        //        component.ExecuteScript(scriptName);
        //    }
        //}

        //public static void StartComponentTasks(string componentName)
        //{
        //    var component = (IComponentActions)GetComponent(componentName);
        //    component.Start();
        //    // this would get the tasks involved 
        //}

        //public static void InvokeComponentEvent(string eventName, params object[] args)
        //{
        //    foreach (var componentName in GetComponents())
        //    {
        //        var component = GetComponent(componentName);  
        //        InvokeEvent(component, eventName, args);
        //    }
        //}

        private static void InvokeComponentEvent(IComponent component, string eventName, object[] args)
        {
            component.EventManager.InvokeEvent(eventName, args);
        }

        private static ComponentStatus GetComponentStatus(string componentName)
        {
            // Need to get the component wrapper 
            if (GetInstance().TryGetComponent(componentName, out var component))
                throw new ArgumentException($"Unable to find a component registered with the name {componentName}");

            return component.Status;
        }

        private bool TryGetComponent(string componentName, out ComponentContainer component)
        {
            return _components.TryGetValue(componentName, out component);
        }

        public IComponent[] InitializeComponents(IEnumerable<ComponentMetadata> componentSettings, ComponentStartProcess startProcess = ComponentStartProcess.SIMULTANEOUS_START, Action<int, string, string> callbackEvent = null)
        { 
            if (!GetInstance().IsHealthy())
                throw new ArgumentException($"Unable to start passed components because not all components have started");

            // We need to remember what components where created, so that in sequence we can load and start all them 
            int[] executionOrder = Util.GetTopologicalSortOrder(componentSettings.ToArray()).Reverse().ToArray();

            // Validating that a component can start, can start is defined as, if a component has a depedency, either it needs to exist in a started state or its part of the passed in components
            var l = componentSettings.Select(e => e.Name).ToList();
            foreach (var item in componentSettings)
            {
                if (item.Dependencies == null)
                    continue;

                for (int i = 0; i < item.Dependencies.Length; i++)
                {
                    var dependency = item.Dependencies[i];
                    if (_components.ContainsKey(dependency))
                    { 
                        continue;
                    }

                    if (l.Contains(dependency))
                    {  
                        continue;
                    }

                    throw new Exception(String.Format("Cannot initalize component {0}, dependent components have been started or are being queued for intialization", item.Name));
                }
            }
             

            // If the components that we alread have, have started, then we must take this list and process it the same way our rule requires
            int stage = 0;
            var newComponents = new List<ComponentContainer>();

            var settingArray = componentSettings.ToArray();
            for (int i = 0; i < executionOrder.Length; i++)
            {
                var componentSetting = settingArray[executionOrder[i]];
                var component = CreateComponent(componentSetting.Assembly, componentSetting.Name);
                RegisterComponent(component);
                newComponents.Add(component); 
            }


            var components = newComponents.ToArray();
            switch (startProcess)
            {
                case ComponentStartProcess.INDIVIDUAL_START:
                    for (int i = 0; i < newComponents.Count; i++)
                    {
                        var component = (IComponentActions)components[executionOrder[i]];

                        callbackEvent?.Invoke(i, ((IComponent)component).Name, "Loading");

                        component.Load();

                        callbackEvent?.Invoke(i, ((IComponent)component).Name, "Starting");

                        component.Start(); 
                    }
                    break;
                case ComponentStartProcess.SIMULTANEOUS_START:
                    while (stage < 3)
                    {
                        switch (stage)
                        { 
                            case 0:
                                {  
                                    for (int i = 0; i < newComponents.Count; i++)
                                    {
                                        var component = (IComponentActions)components[executionOrder[i]];

                                        callbackEvent?.Invoke(i, ((IComponent)component).Name, "Loading");

                                        component.Load();
                                    }
                                }
                                break;
                            case 1:
                                { 
                                    for (int i = 0; i < newComponents.Count; i++)
                                    {
                                        var component = (IComponentActions)components[executionOrder[i]];

                                        callbackEvent?.Invoke(i, ((IComponent)component).Name, "Starting");

                                        component.Start();
                                    }
                                }
                                break;
                        }
                        stage++;
                    }
                    break;
            }
           
             
            // we need to get the resulting wrappers from the new components
            return GetComponents(_components.Keys);
        }

        private void RegisterComponent(ComponentContainer component)
        {
            // next we need to add the container to our manager so we can manupulate or make calls to a component
            AddComponent(component, component.Name);
        } 

        private bool DoesComponentExist(string item)
        {
            return _components.ContainsKey(item);
        }

        private bool IsHealthy()
        {
            var components = _components.Values;
            var componentCheck = components.FirstOrDefault(e => e.Status != ComponentStatus.Started);
            if (componentCheck != null)
                return false;
            return true;
        }

        public static object InvokeComponentAPI(string componentName, string apiName, params object[] args)
        {
            // get the component
            var component = GetComponent(componentName);

            // May get a result back
            var result = component.ApiManager.InvokeAPI(apiName, args);

            return result;
        }

        public static async Task<object> InvokeComponentAPIAsync(string componentName, string apiName, params object[] args)
        {
            // get the component
            var component = GetComponent(componentName);

            // May get a result back
            var result = await component.ApiManager.InvokeAPIAsync(apiName, args);

            return result;
        } 

        private static IComponent[] GetComponents(IEnumerable<string> componentNames)
        {
            var wrappers = new List<IComponent>();
            foreach (var componentName in componentNames)
            {
                // for this, we just call the single version of this and add it to a list
                wrappers.Add(GetComponent(componentName));
            }
            return wrappers.ToArray();
        } 

        internal static IComponent GetComponent(Assembly assembly)
        { 
            return GetComponent(assembly.GetName().Name);
        } 

        public static IComponent GetComponent(string componentName)
        {

            // we need to create a new component wrapper to return back a friendly calling object
            if (!GetInstance().TryGetComponent(componentName, out var component))
                throw new Exception("Component {0} does not exist");
            var wrapper = new ComponentWrapper(componentName);

            // return what we get back
            return wrapper;
        }

        internal ComponentContainer CreateComponent(string assemblyName, string componentName = null, Action<int,string,string> callbackEvent = null)
        {
            if (string.IsNullOrEmpty(assemblyName))
            {
                throw new ArgumentException($"'{nameof(assemblyName)}' cannot be null or empty.", nameof(assemblyName));
            }

            var componentAssembly = Assembly.Load(assemblyName);
            return CreateComponent(componentAssembly);
        }

        internal ComponentContainer CreateComponent(Assembly componentAssembly, Action<int, string, string> callbackEvent = null)
        {
            if (!IsSetup)
                throw new Exception("Component manager has not been created, cannot register a component without a component manager");

            // Some validation
            if (componentAssembly == null)
                throw new ArgumentException("Component assembly is null, unable to start a component with no library to start from");


            // Get me all the types in the assembly that we just loaded, the goal is to find an IComponent to load later
            var types = componentAssembly.GetTypes();

            // We need to find the IComponent as a starting point of the component
            var componentEntryTypes = types.Where(e => e.BaseType == typeof(ComponentBaseImpl));
            if (componentEntryTypes.Count() != 1)
                throw new Exception("A component either has no base implmentation or multiple");

            string componentName = componentAssembly.GetName().Name;

            // we now need to manage this component, the friendly wrapper shouldnt contain the component itself, just a lookup object for the component itself
            var container = new ComponentContainer(GetInstance().ApiManager, GetInstance().EventManager, GetInstance().CommandManager);
            // The managers that are passed are global managers, depending on how this is initialized, they can either be a basic or FiveM manager

            IComponentBase componentBaseInstance = null;

            // We have our entrance
            var componentEntryType = componentEntryTypes.ToArray()[0];

            // Next we need to construct that object
            componentBaseInstance = (IComponentBase)Activator.CreateInstance(componentEntryType);

            // The componet needs a name, by default it will take the name of assembly library its in, but this can be overriden on Impl
            componentName = string.IsNullOrEmpty(componentBaseInstance.Name) ? componentName : componentBaseInstance.Name;

            container.Instance = componentBaseInstance;
            container.Dependencies = componentBaseInstance.Dependencies;

            // If there is a defined component name, we take that, otherwise the IComponent name is set to the assembly name
            container.Name = componentName;
            //var scriptProperties = new Dictionary<string, ComponentScriptProperties>();
            //scriptProperties.Add("InitCore", new ComponentScriptProperties());
            //scriptProperties.Add("InitSession", new ComponentScriptProperties());
            //scriptProperties.Add("RerunScript", new ComponentScriptProperties()
            //{
            //    RerunableScript = true
            //});
            //var scriptConfiguration = new ComponentScriptConfiguration(scriptProperties);


            // we need to make sure that the component is unique in name
            if (GetInstance().DoesComponentExist(componentName))
                throw new ArgumentException($"Component by the name of {componentName} already exists, duplicate names cannot be added. Please add a unqiue named component");


            // We need to find the IComponent as a starting point of the component
            var componentScripts = types.Where(e => e.BaseType == typeof(ComponentScript)).Select(e=>e.FullName).ToArray();
            container.Scripts = componentScripts;


            // Scripts are a more dynamic object, they are called executed and cleaned up
            // Scripts can be rescheduled to rerun

            // TODO load events in assembly

            // TODO load apis in assembly

            // TODO load commands in assembly 

            return container;
        }

        private void AddComponent(ComponentContainer container, string friendlyName)
        {
            _components.Add(friendlyName, container);
        }

        public async Task StartScriptAsync(string v)
        {
            foreach (var item in _components.Values)
            {
                await item.StartScriptAsync(v);
            }
        }
    }
    #endregion
}
