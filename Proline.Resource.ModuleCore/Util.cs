﻿using ComponentFramework.DependencyCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentFramework
{
    internal static class Util
    {
        public static int[] GetTopologicalSortOrder(IComponentData[] fields)
        {
            TopologicalSorter g = new TopologicalSorter(fields.Length);
            Dictionary<string, int> _indexes = new Dictionary<string, int>();

            //add vertices
            for (int i = 0; i < fields.Length; i++)
            {
                _indexes[fields[i].Name.ToLower()] = g.AddVertex(i);
            }

            //add edges
            for (int i = 0; i < fields.Length; i++)
            {
                if (fields[i].Dependencies != null)
                {
                    for (int j = 0; j < fields[i].Dependencies.Length; j++)
                    {
                        g.AddEdge(i,
                            _indexes[fields[i].Dependencies[j].ToLower()]);
                    }
                }
            }

            int[] result = g.Sort();
            return result;

        }
    }
}
