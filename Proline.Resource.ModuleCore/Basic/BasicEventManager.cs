﻿using ComponentFramework.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentFramework.Basic
{
    public class BasicEventManager : ComponentEventManagerImpl
    {
        public override void RegisterEvent(string eventName, Delegate action)
        {
            if (Events.ContainsKey(eventName))
                Events[eventName] = action;
            else
                Events.Add(eventName, action);
        }

        public override void InvokeEvent(string eventName, params object[] args)
        {
            // if an event is not found, we dont bother trying to invoke it.
            if (!Events.ContainsKey(eventName))
                return;
            var eventDel = Events[eventName];
            Task.Factory.StartNew(() =>
            {
                eventDel.DynamicInvoke(args);
            });
        }
    }
}
