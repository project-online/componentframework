﻿using ComponentFramework.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentFramework.Basic
{
    public class BasicCommandManager : ComponentCommandManagerImpl
    {
        public override void RegisterCommand(string commandName, Delegate action)
        {
            if (Commands.ContainsKey(commandName))
                Commands[commandName] = action;
            else
                Commands.Add(commandName, action);
        }

        public override void InvokeCommand(string commandName, params object[] args)
        {
            // if an event is not found, we dont bother trying to invoke it.
            if (!Commands.ContainsKey(commandName))
                return;

            var eventDel = Commands[commandName];
            Task.Factory.StartNew(() =>
            {
                eventDel.DynamicInvoke(args);
            });
        }
    }
}
