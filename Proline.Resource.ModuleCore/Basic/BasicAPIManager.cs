﻿using ComponentFramework;
using ComponentFramework.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentFramework.Basic
{
    public class BasicAPIManager : ComponentAPIManagerImpl
    {
        public override void RegisterAPI(string apiName, Delegate action)
        { 
            APIs.Add(apiName, action);
        }

        public override object InvokeAPI(string apiName, object[] args)
        {

            if (!APIs.ContainsKey(apiName))
                throw new ComponentAPINotFoundException($"API {apiName} not found");

            var api = APIs[apiName];

            var result = api.DynamicInvoke(args);

            return result;
        }
    }
}
