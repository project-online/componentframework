﻿using ComponentFramework; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks; 

namespace ComponentFramework.Basic
{

    public class BasicComponentManager : ComponentManager
    { 

        public BasicComponentManager() : base(new BasicAPIManager(), new BasicEventManager(), new BasicCommandManager())
        {
        }
         
    }
}
