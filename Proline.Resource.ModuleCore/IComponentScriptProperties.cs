﻿namespace ComponentFramework
{
    public interface IComponentScriptProperties
    {
        string Name { get; set; }
        bool RerunableScript { get; set; }
    }
}