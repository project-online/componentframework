﻿using ComponentFramework.Managers;
using System;
using System.Threading.Tasks;

namespace ComponentFramework
{
    public interface IComponent
    {
        string[] Dependencies { get; }
        ComponentStatus Status { get; }
        string Name { get; }
        IComponentAPIManager ApiManager { get; }
        IComponentCommandManager CommandManager { get; }
        IComponentEventManager EventManager { get; }

    }
}