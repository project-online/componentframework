﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentFramework
{
    public interface IComponentBase : IComponentData
    {  
        void OnLoad();

        void OnStart();
    }
}
