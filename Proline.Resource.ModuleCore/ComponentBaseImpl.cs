﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentFramework
{
    public abstract class ComponentBaseImpl : IComponentBase
    {
        private IComponent _component; 
        public IComponent Component { get {
                if (_component == null)
                    _component = ComponentManager.GetComponent(Name);
                return _component;
            } }
        public string[] Dependencies { get;  }
        public string Name { get; } 
        public string Assembly { get; }

        protected ComponentBaseImpl(string name, params string[] depedencies)
        {
            Name = name;
            Dependencies = depedencies;
        }

        public abstract void OnLoad();

        public abstract void OnStart();
    }
}
