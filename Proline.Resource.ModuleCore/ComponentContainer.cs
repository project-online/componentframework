﻿using ComponentFramework.Managers; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentFramework
{
    internal class ComponentContainer : IComponent, IComponentActions
    { 
        public IComponentBase Instance { get; internal set; }
        public ComponentStatus Status { get; set; } 
        public string Name { get; internal set; } 
        public string[] Dependencies { get; internal set; }
        public string[] Scripts { get; internal set; }
        public IComponentAPIManager ApiManager { get; }
        public IComponentCommandManager CommandManager { get; }
        public IComponentEventManager EventManager { get; }
         

        /// <summary>
        /// Component container takes in 3 managers, api, events and commands. These need to be set to something or the features of this component wont work
        /// </summary>
        /// <param name="apiManager"></param>
        /// <param name="eventManager"></param>
        /// <param name="commandManager"></param>
        public ComponentContainer(IComponentAPIManager apiManager, IComponentEventManager eventManager, IComponentCommandManager commandManager)
        {
            ApiManager = apiManager;
            EventManager = eventManager;
            CommandManager = commandManager; 
        }
         
        public void Load()
        {  
            // Call the instance if it exists and its OnLoad method to give the programmer a chance to do addional things
            if (Instance != null)
                Instance.OnLoad(); 

            Status = ComponentStatus.Loaded;
        }

        public void Start()
        {
            // Call the instance if it exists and its OnStart method to give the programmer a chance to do addional things
            if(Instance == null)
                Instance.OnStart(); 

            Status = ComponentStatus.Started;
        }
         
       
        /// <summary>
        /// Execute script will perform a direct execute call on a passed script
        /// </summary>
        /// <param name="script"></param>
        private void ExecuteScript(ComponentScript script)
        {
            script.OnExecute();
        }

        /// <summary>
        /// Start script async will perform a task base function of executing a script
        /// </summary>
        /// <param name="scriptName"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task StartScriptAsync(string scriptName)
        {
            ComponentScript script = null;
            foreach (var s in Scripts)
            {
                var type = Type.GetType(s);
                if (type.Name.Equals(scriptName))
                {
                    script = (ComponentScript)Activator.CreateInstance(type);
                    break;
                }
            }

            if (script == null)
                throw new Exception("Failed to create new script from script name " + scriptName);

            await Task.Factory.StartNew(() =>
            {
                ExecuteScript(script);
            }); 
        }
    }
}
