﻿namespace ComponentFramework
{
    public class ComponentScriptProperties : IComponentScriptProperties
    {
        public bool RerunableScript { get; set; }
        public string Name { get; set; }
    }
}