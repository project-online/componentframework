﻿using System;
using System.Runtime.Serialization;

namespace ComponentFramework
{
    [Serializable]
    public class ComponentAPINotFoundException : Exception
    {
        public ComponentAPINotFoundException()
        {
        }

        public ComponentAPINotFoundException(string message) : base(message)
        {
        }

        public ComponentAPINotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ComponentAPINotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}