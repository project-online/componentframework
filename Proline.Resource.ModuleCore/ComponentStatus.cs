﻿namespace ComponentFramework
{
    public enum ComponentStatus
    {
        Created,
        Loaded,
        Started
    }
}