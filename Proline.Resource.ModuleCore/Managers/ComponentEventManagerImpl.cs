﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentFramework.Managers
{
    public abstract class ComponentEventManagerImpl : IComponentEventManager
    {
        private Dictionary<string, Delegate> _events;
        public Dictionary<string, Delegate> Events
        {
            get
            {
                if (_events == null)
                    _events = new Dictionary<string, Delegate>();
                return _events;
            }
        }

        public abstract void InvokeEvent(string eventName, params object[] args); 
        public abstract void RegisterEvent(string eventName, Delegate action);
    }
}
