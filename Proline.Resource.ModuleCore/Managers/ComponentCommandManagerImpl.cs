﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentFramework.Managers
{
    public abstract class ComponentCommandManagerImpl : IComponentCommandManager
    {
        private Dictionary<string, Delegate> _commands;
        public Dictionary<string, Delegate> Commands
        {
            get
            {
                if (_commands == null)
                    _commands = new Dictionary<string, Delegate>();
                return _commands;
            }
        }

        public abstract void InvokeCommand(string commandName, params object[] args);

        public abstract void RegisterCommand(string commandName, Delegate action);
    }
}
