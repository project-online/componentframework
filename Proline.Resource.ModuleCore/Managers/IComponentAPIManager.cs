﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ComponentFramework.Managers
{
    public interface IComponentAPIManager
    {
        Dictionary<string, Delegate> APIs { get; }
        void RegisterAPI(Delegate action);
        void RegisterAPI(string apiName, Delegate action);
        object InvokeAPI(string apiName, object[] args);
        Task<object> InvokeAPIAsync(string apiName, object[] args);
    }
}