﻿using System;

namespace ComponentFramework.Managers
{
    public interface IComponentEventManager
    {
        void RegisterEvent(string eventName, Delegate action);
        void InvokeEvent(string eventName, params object[] args);
    }
}