﻿using System;

namespace ComponentFramework.Managers
{
    public interface IComponentCommandManager
    {
        void RegisterCommand(string commandName, Delegate action);
        void InvokeCommand(string commandName, params object[] args);
    }
}