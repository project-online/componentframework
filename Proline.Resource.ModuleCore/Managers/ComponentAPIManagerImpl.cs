﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ComponentFramework.Managers
{
    public abstract class ComponentAPIManagerImpl : IComponentAPIManager
    {
        private Dictionary<string, Delegate> _apis; 
        public Dictionary<string, Delegate> APIs { 
            get
            {
                if(_apis == null)
                    _apis = new Dictionary<string, Delegate>();
                return _apis;
            } 
        }

        public abstract object InvokeAPI(string apiName, object[] args);

        public Task<object> InvokeAPIAsync(string apiName, object[] args)
        {
            return (Task<object>)InvokeAPI(apiName, args);
        }
        public void RegisterAPI(Delegate action)
        {
            RegisterAPI(action.Method.Name, action);
        }

        public abstract void RegisterAPI(string apiName, Delegate action);
    }
}