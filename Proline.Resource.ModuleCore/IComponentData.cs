﻿namespace ComponentFramework
{
    public interface IComponentData
    {
        string Assembly { get; }
        string[] Dependencies { get; }
        string Name { get; }
    }
}