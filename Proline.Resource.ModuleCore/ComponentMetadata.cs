﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentFramework
{
    public class ComponentMetadata : IComponentData
    {
        public string[] Dependencies { get; set; }

        public string Name { get; set; }

        public string Assembly { get; set; }
    }
}
