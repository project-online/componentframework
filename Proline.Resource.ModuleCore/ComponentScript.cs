﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentFramework
{
    public class ComponentScriptConfiguration
    { 
        private Dictionary<string, ComponentScriptProperties> _scriptProperties;

        public ComponentScriptConfiguration(Dictionary<string, ComponentScriptProperties> scriptProperties)
        {
            _scriptProperties = scriptProperties;
        }

        internal IEnumerable<string> GetOrder()
        {
            return _scriptProperties.Keys;
        }

        internal ComponentScriptProperties GetProperties(string item)
        {
            return _scriptProperties[item];
        }
    }

    public abstract class ComponentScript : IComponentScriptProperties
    {
        protected ComponentScript()
        {
            Name = GetType().Name;
            RerunableScript = false;
        }

        protected ComponentScript(string name, bool rerunableScript)
        {
            Name = name;
            RerunableScript = rerunableScript;
        }

        public string Name { get;  set; }
        public bool RerunableScript { get; set; }

        public abstract Task OnExecute();
    }
}
