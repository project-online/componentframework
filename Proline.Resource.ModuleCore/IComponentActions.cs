﻿using System.Threading.Tasks;

namespace ComponentFramework
{
    internal interface IComponentActions
    { 
        void Start();
        void Load();
        Task StartScriptAsync(string scriptName);
    }
}