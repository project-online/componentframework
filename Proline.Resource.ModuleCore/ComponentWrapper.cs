﻿using ComponentFramework.Managers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ComponentFramework
{
    public class ComponentWrapper : IComponent
    { 
        public string[] Dependencies => Component.Dependencies;
        public string Name => Component.Name;
        public ComponentStatus Status => Component.Status;
        public IComponentAPIManager ApiManager => Component.ApiManager;
        public IComponentCommandManager CommandManager => Component.CommandManager;
        public IComponentEventManager EventManager => Component.EventManager;
        internal IComponent Component { get
            {
                // This is all to ensure that the static manager has complete control over the component object
                // if the manager says its gone, then all wrapper objects must also abandon managing it
                if (!_components.ContainsKey(_componentName))
                {
                    _component = null;
                    if (_component == null)
                        throw new ComponentNotFoundException();
                    return null;
                }
                else
                { 
                    if (_component == null)
                    {
                        _components.TryGetValue(_componentName, out _component);
                    }
                    return _component;
                }
            }
        } 


        private string _componentName;
        private ComponentContainer _component;
        // Need to move this to a manager
        private Dictionary<string, ComponentContainer> _components => ComponentManager.GetInstance().Components;


        /// <summary>
        /// This is a wrapper for the component container, ensured that if a component is removed, calls to it would be null
        /// </summary>
        /// <param name="component"></param>
        internal ComponentWrapper(string component)
        {
            _componentName = component; 
        } 
    }
}
