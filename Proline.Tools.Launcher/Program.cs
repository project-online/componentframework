﻿using ComponentFramework;
using ComponentFramework.Basic;
using ComponentLoader.Config;
using log4net;
using System.Collections.Generic;
using System.Linq;

namespace ComponentLoader
{
    public class Program
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void Main(string[] args)
        {
            var manager = new BasicComponentManager();
            var components = new List<ComponentMetadata>();

            foreach (ComponentInstanceElement item in ComponentConfigSection.ComponentConfig.Components)
            {
                components.Add(new ComponentMetadata()
                {
                    Assembly = item.Name,
                    Name = item.Name,
                });
            }
             

            components[0].Dependencies = new string[] { "ExampleComponent2" };




            manager.InitializeComponents(components, ComponentStartProcess.SIMULTANEOUS_START, (i, name, action) =>
            {
                var s = string.Format("[{0}/{1}]{2} {3} ", i + 1, components.Count(), action, name);
                log.Info(s);
            });


            ComponentManager.InvokeComponentEvent("EventHandler", "An example of a component invoking an event on a global level");
            //ComponentManager.GetComponent("ExampleComponent").EventManager.InvokeEvent("EventHandler2", "An example of a component invoking an event on a local level");
            ComponentManager.InvokeComponentAPI("ExampleComponent", "WowAPI2");
            ComponentManager.InvokeComponentEvent("TestEvent");
            ComponentManager.InvokeComponentCommand("ExampleComponent", "CommandExample", "Example Command");
        }
    }
}
