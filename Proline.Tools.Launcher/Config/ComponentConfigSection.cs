﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComponentLoader.Config
{
    public class ComponentConfigSection : ConfigurationSection
    {
        public static ComponentConfigSection ComponentConfig => (ComponentConfigSection)ConfigurationManager.GetSection("componentConfigSection");

        // Create a property that lets us access the collection
        // of SageCRMInstanceElements

        // Specify the name of the element used for the property
        [ConfigurationProperty("componentInstances")]
        // Specify the type of elements found in the collection
        [ConfigurationCollection(typeof(ComponentInstanceCollection))]
        public ComponentInstanceCollection Components
        {
            get
            {
                // Get the collection and parse it
                return (ComponentInstanceCollection)this["componentInstances"];
            }
        }
    }
}
