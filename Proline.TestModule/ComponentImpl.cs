﻿using ComponentFramework;
using ExampleComponent.Natives;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleComponent
{
    public class ComponentImpl : ComponentBaseImpl
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ComponentImpl() : base("ExampleComponent")
        { 
        }

        public override void OnLoad()
        {
            // All component do a form of loading when registered

            // Here is an example of registering an API
            Component.ApiManager.RegisterAPI("ExampleReturn", new Func<string, object>((args) =>
             {
                // API stuff
                return "WowSomethingCameback";
             }));

            Component.ApiManager.RegisterAPI("ExampleReturnTask", new Func<Task<object>>(async () =>
            {
                // API stuff
                await Task.Delay(1000);

                return "SomethingBack";
            }));

            // Example of a API using lambda
            Component.ApiManager.RegisterAPI("WowAPI", new Action(() =>
           {
               log.Debug("API Called!!!!");
           }));

            // Example of an API delegate
            Component.ApiManager.RegisterAPI("WowAPI2", new Action(ExampleComponentAPIs.WowAPI));

            Component.EventManager.RegisterEvent("EventHandler", new Action<string>((arg) =>
            {
                log.Debug(arg);
            }));

            Component.EventManager.RegisterEvent("EventHandler2", new Action<string>((arg) =>
            {
                log.Debug(arg + "dsadsadsad");
            }));

            Component.CommandManager.RegisterCommand("CommandExample", new Action<string>((arg) => { 
                log.Debug("Command executed" + arg);
            }));
        }


        public override void OnStart()
        {
            // Once all components are loaded, all components simulatiously start

            // Example invoke
            //Component.InvokeEvent("EventHandler", "An example of a component invoking an event on a container level");
        }
    }
}
