﻿using ComponentFramework;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleComponent.Natives
{
    public static class ExampleComponentAPIs
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly IComponent component = ComponentManager.GetComponent("ExampleComponent");
        public static void WowAPI()
        {
            component.EventManager.RegisterEvent("TestEvent", new Action(() =>
            {
                log.Debug("Seee, it does work");
            }));
            component.EventManager.InvokeEvent("EventHandler", "I just called an event from an API");
             
        }
    }
}
